import React from 'react';
import classNames from 'classnames';
import { Link, NavLink } from 'react-router-dom';
import { setUserDetails,setJWT,isAuthenticate, getUserDetails } from '../../_helper/authentication';
import './sidebar.scss';
import Account from '../account';
import Subscribe from '../../components/subscibe';
// import leaderboard from  '../../assets/icon/icon-1.png';
// import reward from  '../../assets/icon/icon-2.png';
import AccountImg from '../../assetsStaging/img/account.svg';
import contest from '../../assetsStaging/img/sidenav/contest.png';
import reward from '../../assetsStaging/img/sidenav/reward.png';
import leaderboard from '../../assetsStaging/img/sidenav/leaderboard.png';
import winners from '../../assetsStaging/img/sidenav/winners.png';
import Language from '../../assetsStaging/img/sidenav/language.png';
import faq from '../../assetsStaging/img/sidenav/faq.png';
import logout from '../../assetsStaging/img/sidenav/logout.png';
import privacypolicy from '../../assetsStaging/img/sidenav/privacypolicy.png';
import term from '../../assetsStaging/img/sidenav/term.png';
import enter from '../../assetsStaging/img/sidenav/enter.png';

//import { useHistory } from "react-router";

import {Modal,Button} from 'react-bootstrap';
import {useState} from 'react';
import axios from '../../_config/axios'
import './Sidebar.css';

const Sidebar = ({ open, closeSideBar }) => {
	const language = { "en": "English", "id": "Indonesia", "ms": "Malaysia", "nl": "Deutch", 'km': 'Khmer' };
	//const history = useHistory();

	const [panel, setPanel] = useState(false);
	const [msisdn,setmsisdn]= useState("");
	const [errmsisdn,seterrmsisdn]= useState("");
	const [pin,setpin] = useState(false);
	const[otp,setotp]= useState("");
	const[otperr,setotperr]= useState("");
    const[subpin,setsubpin]= useState(false) ;
	const[subscr,setsubscr]=useState("Unsubscribe");

    

	const logOut = () => {
		console.log('logoutt')
		localStorage.removeItem('userDetails');
		localStorage.removeItem('JWT');
		localStorage.clear();
		// localStorage.removeItem('userDetailsforPopup');
	}
	//console.log(isAuthenticate());
	//console.log(localStorage.getItem('msisdns'));
	let msisdns=  localStorage.getItem('msisdns');
	//console.log(msisdns)

	function toggleButton () {
		if(!panel) setPanel(true);
		else setPanel(false);
		seterrmsisdn("");
		}


		function handleChange(e) {
			//console.log(e.target.value);
			setmsisdn(e.target.value)

		  }

		  function msisdnhit(){
			  console.log(msisdn)
			  var apiurl="/Subscription/subcribed?msisdn="+msisdn;
			  console.log(apiurl)
			  const payload = new FormData();
              payload.append('msisdn',msisdn)
			  
             //console.log(apiurl,payload)
			     axios.get(apiurl).then(res=>{
				 console.log(res.data)
				 if(res.data.msg === "subscribe success"){
				 //if(res.data.response === true){
					 setotperr("");
					setpin(true);
					 setPanel(false);
					 
					 
				 }
				 else{
					seterrmsisdn("Invalid MSISDN or MSISDN already Subscribed")
				 }
			 }).catch(err=>{
				 console.log("error")
			 })
		  }

          function handleotpChange(e) {
			//console.log(e.target.value);
			setpin(e.target.value)

		  }

		  function otphit(){
			  console.log(pin)
			  console.log(msisdn)
			  const payload = new FormData();
        let url = "Subscription/confirmation?msisdn="+msisdn+"&pin="+pin+"&service="
        console.log(url)
        axios.get(url).then(res=>{
            console.log(res)
			if(res.data.response=== true){
				setUserDetails(res.data);
            //setJWT(2059431100);
            setJWT(res.data.msisdn);
                localStorage.setItem('userDetails', JSON.stringify(res.data));
                localStorage.setItem('response',JSON.stringify(res.data.response));
                localStorage.setItem('status',JSON.stringify(res.data.status));
				localStorage.setItem('msisdns',JSON.stringify(res.data.msisdn));
				window.location.href="/"
				//setpin("")
				//setUserUuid(msisdn)
				//setJWT(msisdn);
				 
				
				// localStorage.clear();
				// setUserDetails(msisdn);
				// setJWT(msisdn);
				// localStorage.setItem('msisdns',JSON.stringify(msisdn));
				// localStorage.setItem('msisdnnnn',JSON.stringify(msisdn));
				// window.location.href="/"
               //setotperr("Invalid OTP")
			}
			else{
				setotperr("Invalid OTP")
			}
        }).catch(res=>{
            console.log("Error")
        })
		  }




		  function toggleButtonss(){
			  setpin("");
		  }
            
		    function funsubtog(){
				let unsuburl= "Subscription/unsubcrition?msisdn="+msisdns.substring(1,msisdns.length-1)
				const payload = new FormData();
				payload.append('msisdn',msisdns)
				axios.get(unsuburl).then(res=>{

					console.log(res)
					if(res.data.status==="TRUE"){
						localStorage.clear();
						window.location.href="/"
						//console.log("logour")
					}
					
					//setsubscr("Subscribe");
				}).catch(err=>{
					console.log("error")
				})
				// if(!subpin){ 
					
		            
				// 	console.log(unsuburl)
				// 	setsubscr("Subscribe");
				// 	setsubpin(true);
				// }
		// else{ 
		// 	   setsubscr("Unsubscribe")
		// 	   setsubpin(false);
		// }
		       //seterrmsisdn("");
			
	   }

	   function loginunsubs(){
		   let loginmsisdn = localStorage.getItem('msisdns').substring(1,localStorage.getItem('msisdns').length-1);
		   console.log(loginmsisdn);
		   let unsuburl= "Subscription/unsubcrition?msisdn="+loginmsisdn;
				//const payload = new FormData();
				//payload.append('msisdn',loginmsisdns)
				axios.get(unsuburl).then(res=>{

					console.log(res)
					if(res.data.status==="TRUE"){
						localStorage.clear();
						window.location.href="/"
						
					}
					
					//setsubscr("Subscribe");
				}).catch(err=>{
					console.log("error")
				})
	   }
	   function loginsubscription(){
		   //setmsisdn(local)
		   let loginmsisdn = localStorage.getItem('msisdns').substring(1,localStorage.getItem('msisdns').length-1);
		   setmsisdn(loginmsisdn);
		   console.log(msisdn)
			  var apiurl="/Subscription/subcribed?msisdn="+loginmsisdn;
			  console.log(apiurl)
			  const payload = new FormData();
              payload.append('msisdn',msisdn)
			  
             //console.log(apiurl,payload)
			     axios.get(apiurl).then(res=>{
				 console.log(res.data)
				 if(res.data.msg === "subscribe success"){
				 //if(res.data.response === true){
					 setotperr("");
					setpin(true)
					 setPanel(false);
					 
					 
                 }}).catch(error=>{
					 console.log(error);
				 });
		
		   

	   }
		    let status= localStorage.getItem('status');
			//console.log(status)
			let msisdnnnn = localStorage.getItem('msisdnnnn');
			//console.log(msisdnnnn)
	return (
		<>
			{/* <Subscribe show={show} handleClose={this.handleClose}/> */}
			{open && <div style={{
				position: 'fixed',
				top: 0,
				left: 0,
				right: 0,
				bottom: 0,
				background: 'rgba(0,0,0,0.8)',
				zIndex: 1000
			}} onClick={closeSideBar}></div>}
			<nav className={classNames("sideNav", "bgImg2", { open: open })} style={{ overflow: 'hidden' }}>
				<div className="wrapper" style={{backgroundColor:'#D1171B'}}>
					<div id="sidenavmenu" style={{ display: 'block' }}>
						<div className="sidenav" style={{
							backgroundColor: ' #D1171B',
							width: '300px',
							height: '100%',
							overflowX: 'hidden',
							paddingTop: 0,
							textAlign: 'initial',
						}}>
							<div className="sidenav-header block">
								<img src={isAuthenticate() === true ? getUserDetails().image : AccountImg } alt="" />
								{isAuthenticate() === true ?
									<div className="my-1 text-white">
										<span className="d-block">{getUserDetails().first_name}</span>
										<span className="d-block">{getUserDetails().phone_no}</span>
									</div>
									:
									<div className="my-1 text-white">
										<span className="d-block">Demo Goaly</span>
										{/* <span className="d-block">08129545XXXX</span> */}
									</div>
								}
								
								{msisdns && msisdns.length>=1 && <p style={{color:'white',fontWeight:700}}>{msisdns.substring(1,msisdns.length-1)}</p>}
								{status && (status==='"active"')&&<p style={{color:'white'}}>active</p>}
								{status && (status==='"inactive"') && <p style={{color:'white'}}>inactive</p>}
								{msisdnnnn && msisdnnnn.length>=1  && <p style={{color:'white'}}>active</p>}
                                {msisdnnnn && msisdnnnn.length>=1 ?<button className="btn btn-pill btn-blue w-75 mt-1" onClick={funsubtog}><b>{subscr}</b></button>:
								null }
								{status && (status==='"active"')&& <button className="btn btn-pill btn-blue w-75 mt-1" onClick={loginunsubs}><b>Unsubscribe</b></button>}
								{!msisdnnnn && !status && !msisdns && <button className="btn btn-pill btn-blue w-75 mt-1" onClick={toggleButton}><b>Subscribe</b></button>}
								{status && (status==='"inactive"') && <button className="btn btn-pill btn-blue w-75 mt-1" onClick={loginsubscription}>Subscribe</button>}
								{/* <button className="btn btn-pill btn-blue w-75 mt-1" onClick={toggleButton}><b>Subscibe</b></button> */}
							</div>
							{/* <Modal show={true}>
								<br/><br/><br/>
								<Modal.Header>Modal Head Part</Modal.Header>
								<Modal.Body>Hi ,React Modal is hear</Modal.Body>
								<Modal.Footer><button>Close Modal</button></Modal.Footer>
							</Modal> */}
							{/* <Modal.Dialog>
                         <Modal.Header>
                        <Modal.Title>Modal title</Modal.Title>
                       </Modal.Header>

                      <Modal.Body>One fine body...</Modal.Body>

                          <Modal.Footer>
                          <Button>Close</Button>
                        <Button bsStyle="primary">Save changes</Button>
                                </Modal.Footer>
                               </Modal.Dialog> */}
							   {panel && <div>
							    <div /*className="static-modal"*/>
                                  <Modal.Dialog style={{top:302,width:'113%',position :'fixed',marginLeft:'-25px'}} > 
                                     <Modal.Header style={{backgroundColor:'white'}}>
                                <Modal.Title style={{textAlign:'center',fontWeight:"bolder"}} >Enter msisdn</Modal.Title>
                                  </Modal.Header>

                                     <Modal.Body style={{backgroundColor:'white',textAlign:'center'}}><input style={{textAlign:'center',border:'1px solid black',borderRadius:'13px',height:34}} type="text" onChange={handleChange}/>
									 {errmsisdn && <p style={{textAlign:'center',fontWeight:500,color:'red'}}>{errmsisdn}</p>}
									 {/* <Button onClick={toggleButton}>Cancel</Button>
                                        <Button bsStyle="red" onClick={msisdnhit}>Submit</Button> */}
									 </Modal.Body>

                                            <Modal.Footer>
                                          <Button onClick={toggleButton}>Cancel</Button>
                                        <Button bsStyle="red" onClick={msisdnhit}>Submit</Button>
                                                 </Modal.Footer>
                                              </Modal.Dialog>
                                               </div>
											   </div>}


											   {pin && <div className="abc">
							    <div /*className="static-modal"*/>
                                  <Modal.Dialog style={{top:302,width:'113%',position :'fixed',marginLeft:'-25px'}} > 
                                     <Modal.Header style={{backgroundColor:'white'}}>
                                <Modal.Title style={{textAlign:'center'}}>Enter OTP</Modal.Title>
                                  </Modal.Header>

                                     <Modal.Body style={{backgroundColor:'white',textAlign:'center'}}><input type="text" style={{textAlign:'center',border:'1px solid black',height:30,borderRadius:12}} onChange={handleotpChange}/>
									 {otperr && <p style={{textAlign:'center',fontWeight:500,color:'red'}}>{otperr}</p>}
									 </Modal.Body>

                                            <Modal.Footer>
                                          <Button onClick={toggleButtonss}>Cancel</Button>
                                        <Button bsStyle="red" onClick={otphit}>Submit</Button>
                                                 </Modal.Footer>
                                              </Modal.Dialog>
                                               </div>
											   </div>}
							   
          
							
							<ul className="my-2">

								{!isAuthenticate() ?
									<li><NavLink to="/login" onClick={closeSideBar}><img src={enter} alt="" />Login</NavLink></li>
									:
									''
								}
								<li><NavLink to="/contest" onClick={closeSideBar}><img src={contest} alt="" /> Contest</NavLink></li>

								<li><NavLink to="/reward" onClick={closeSideBar}><img src={reward} alt="" /> Rewards</NavLink></li>
								<li><NavLink to="/leaderboard" onClick={closeSideBar}><img src={leaderboard} alt="" /> Leaderboard</NavLink></li>
								<li><NavLink to="/winner" onClick={closeSideBar}><img src={winners} alt="" />Winners</NavLink></li>
								<li><NavLink to="/language" onClick={closeSideBar}><img src={Language} alt="" /> Language <span id="language">{language[selectedLanguage()]}</span></NavLink></li>
								<li><NavLink to="/faq" onClick={closeSideBar}><img src={faq} alt="" /> FAQ</NavLink></li>
								<li><NavLink to="/privacy" onClick={closeSideBar}><img src={privacypolicy} alt="" />Privacy policy</NavLink></li>
								<li><NavLink to="/service" onClick={closeSideBar}><img src={term} alt="" />Terms of Service</NavLink></li>
								{isAuthenticate() ?
									<li><NavLink to="" onClick={logOut}><img src={logout} alt="" /> Logout</NavLink></li>
									:
									''
								}
								{/* <li><NavLink to="/login" onClick={closeSideBar}>New Login</NavLink></li> */}

							</ul>
						</div>
					</div>
				</div>
			</nav>
		</>
	);
}

export default Sidebar;

const selectedLanguage = () => {
	var name = 'googtrans';
	var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
	if (match) return match[2].split('/')[2];
	return 'en';
}

{/* <div className="notop bg-main">
				<Link to='/profile' onClick={closeSideBar}><Account /></Link>
					<div className="list-block mt-15">
						<div className="list-group">
							<nav>
								<div className="list-block">
									<ul>
										<li className="divider" style={{ marginBottom: '6px' }}>Menu</li>
										<li>
											{isAuthenticate() ?
												<NavLink exact to='/' className="item-link close-panel item-content" activeclassname="active-side-bar" onClick={closeSideBar}>
													<div className="item-media"><i className="fa fa-home"></i></div>
													<div className="item-inner">
														<div className="item-title">Home</div>
													</div>
												</NavLink>
												:
												<NavLink to='/login' className="item-link close-panel item-content" activeclassname="active-side-bar" onClick={closeSideBar}>
													<div className="item-media"><i className="fa fa-sign-in"></i></div>
													<div className="item-inner">
														<div className="item-title">Login </div>
													</div>
												</NavLink>
											}
										</li>
										<li>
											<NavLink to="/contest" className="item-link close-panel item-content" activeclassname="active-side-bar" onClick={closeSideBar}>
												<div className="item-media"><i className="fa fa-bookmark"></i></div>
												<div className="item-inner">
													<div className="item-title">Contest</div>
												</div>
											</NavLink>
										</li>
										<li>
											<NavLink to="/reward" className="item-link close-panel item-content" activeclassname="active-side-bar" onClick={closeSideBar}>
												<div className="item-media">
													<img src={reward} style={{height:15}}/>

													</div>
												<div className="item-inner">
													<div className="item-title">Rewards </div>
												</div>
											</NavLink>
										</li>
										<li>
											<NavLink to="/leaderboard" className="item-link close-panel item-content" activeclassname="active-side-bar" onClick={closeSideBar}>
												<div className="item-media">
													<img src={leaderboard} style={{height:15}}/>
													</div>
												<div className="item-inner">
													<div className="item-title">Leaderboard</div>
												</div>
											</NavLink>
										</li>

										<li>
											<NavLink to="/winner" className="item-link close-panel item-content" activeclassname="active-side-bar" onClick={closeSideBar}>
												<div className="item-media"><i className="fa fa-trophy"></i></div>
												<div className="item-inner">
													<div className="item-title">Winners</div>
												</div>
											</NavLink>
										</li>

										<li>
											<NavLink to="/language" className="item-link close-panel item-content" activeclassname="active-side-bar" onClick={closeSideBar}>
												<div className="item-media">
													<i className="fa fa-globe"></i>
												</div>
												<div className="item-inner">
													<div className="item-title">Language</div>
													<div className="item-after">{language[selectedLanguage()]}</div>
												</div>
											</NavLink>
										</li>
										<li>
											{isAuthenticate() &&
												<Link to='/logout' className="item-link close-panel item-content" onClick={closeSideBar}>
													<div className="item-media"><i className="fa fa-sign-out"></i></div>
													<div className="item-inner">
														<div className="item-title">Logout</div>
													</div>
												</Link>}
										</li>
										<li className="divider" style={{
											marginTop: '10px',
											marginBottom: '10px'
										}}></li>
										<li>
											<NavLink to="/faq" className="item-link close-panel item-content" activeclassname="active-side-bar" onClick={closeSideBar}>
												<div className="item-media"><i className="fa fa-question-circle"></i></div>
												<div className="item-inner">
													<div className="item-title">FAQ</div>
												</div>
											</NavLink>
										</li>
										<li>
											<NavLink to="/privacy" className="item-link close-panel item-content" activeclassname="active-side-bar" onClick={closeSideBar}>
												<div className="item-media"><i className="fa fa-question-circle"></i></div>
												<div className="item-inner">
													<div className="item-title">Privacy Policy</div>
												</div>
											</NavLink>
										</li>
										<li>
											<NavLink to="/service" className="item-link close-panel item-content" activeclassname="active-side-bar" onClick={closeSideBar}>
												<div className="item-media"><i className="fa fa-question-circle"></i></div>
												<div className="item-inner">
													<div className="item-title">Terms of Service</div>
												</div>
											</NavLink>
										</li>
										<li className="divider" style={{
											marginTop: '10px',
											marginBottom: '10px'
										}}></li>
										<li>
											<a className="item-link close-panel item-content" activeclassname="active-side-bar" onClick={() => true}>
												<div className="item-media"><i className="fa fa-question-circle"></i></div>
												<div className="item-inner">
													<div className="item-title">Subscribe</div>
												</div>
											</a>
										</li>
									</ul>
								</div>
							</nav>
						</div>
					</div>
				</div> */}


